package com.upwork.cloudy.reports.logger.services.impl;

import com.google.firebase.database.DatabaseReference;
import com.upwork.cloudy.reports.domain.models.ApiEvent;
import com.upwork.cloudy.reports.logger.services.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LogServiceImpl implements LogService<ApiEvent> {

    private final DatabaseReference ref;

    @Value("${firebase.path.child}")
    private String childPath;

    private static final Logger LOG = LoggerFactory.getLogger(LogServiceImpl.class);

    @Autowired
    public LogServiceImpl(DatabaseReference ref) {
        this.ref = ref;
    }

    @Override
    public void save(ApiEvent apiEvent) {
        LOG.info("Saving event to firebase {}", apiEvent);

        final DatabaseReference childRef = ref.child(childPath);
        childRef.child(apiEvent.getId()).setValueAsync(apiEvent);
    }

    @Override
    public void update(ApiEvent apiEvent) {
        LOG.info("Updating event to firebase {}", apiEvent);

        final DatabaseReference childRef = ref.child(childPath);

        final Map<String, Object> apiEventsUpdates = new HashMap<>();
        apiEventsUpdates.put(apiEvent.getId(), apiEvent);

        childRef.updateChildrenAsync(apiEventsUpdates);
    }

    public DatabaseReference getRef() {
        return ref;
    }
}
