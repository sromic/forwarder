package com.upwork.cloudy.reports.logger.controllers;

import com.upwork.cloudy.reports.domain.models.ApiEvent;
import com.upwork.cloudy.reports.logger.errors.FirebaseValidationError;
import com.upwork.cloudy.reports.logger.services.LogService;
import com.upwork.cloudy.reports.logger.validators.impl.FirebasePayloadValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Optional;

@RestController
public class LogController {

    private final LogService<ApiEvent> logService;

    private final FirebasePayloadValidator firebasePayloadValidator;

    @Autowired
    public LogController(LogService<ApiEvent> logService, FirebasePayloadValidator firebasePayloadValidator) {
        this.logService = logService;
        this.firebasePayloadValidator = firebasePayloadValidator;
    }

    @PostMapping("/log")
    public ResponseEntity<?> logData(@NotNull @RequestBody final ApiEvent apiEvent) {

        //validate if response payload valid before saving to firebase
        if (apiEvent.getResponse() != null) {
            final Optional<Map<String, String>> errors = firebasePayloadValidator.validate(apiEvent.getResponse().getBody());
            if (errors.isPresent())
                throw new FirebaseValidationError("Error while validating Firebase response payload", null, errors.get());

        }

        logService.save(apiEvent);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public LogService<ApiEvent> getLogService() {
        return logService;
    }
}
