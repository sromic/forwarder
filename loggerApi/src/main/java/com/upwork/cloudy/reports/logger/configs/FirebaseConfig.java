package com.upwork.cloudy.reports.logger.configs;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;

@Configuration
public class FirebaseConfig {

    @Value("${firebase.path}")
    private String path;

    @Value("classpath:forwardapi-firebase.json")
    private Resource gservicesConfig;

    @Value("${firebase.database-url}")
    private String databaseUrl;

    @Bean
    public FirebaseApp provideFirebaseOptions() throws IOException {
        final FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(gservicesConfig.getInputStream()))
                .setDatabaseUrl(databaseUrl)
                .build();

        return FirebaseApp.initializeApp(options);
    }

    @Bean
    public DatabaseReference provideDatabaseReference(FirebaseApp firebaseApp) {
        FirebaseDatabase
                .getInstance(firebaseApp)
                .setPersistenceEnabled(false);
        return FirebaseDatabase
                .getInstance(firebaseApp)
                .getReference(path);
    }
}
