package com.upwork.cloudy.reports.logger.validators.impl;

import com.upwork.cloudy.reports.logger.validators.Validator;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public final class FirebasePayloadValidator implements Validator<Map<String, Object>, Optional<Map<String, String>>> {

    private final String[] invalidCharacters = new String[]{".", "$", "[", "]", "#", "/"};

    @Override
    public Optional<Map<String, String>> validate(final Map<String, Object> payload) {
        final List<String> invalidKeysStream = payload
                .keySet()
                .stream()
                .filter((String key) -> {
                    if (payload.get(key) instanceof Map)
                        return validate((Map<String, Object>) payload.get(key)).isPresent();
                    else
                        return Arrays
                                .stream(invalidCharacters)
                                .anyMatch(invalidCharacter -> key.contains(invalidCharacter));
                })
                .collect(Collectors.toList());

        if (!invalidKeysStream.isEmpty())
            return Optional.ofNullable(invalidKeysStream
                    .stream()
                    .collect(Collectors.toMap(key -> key, value -> "Keys must not contain '/', '.', '#', '$', '[', or ']'"))
            );

        return Optional.empty();
    }
}
