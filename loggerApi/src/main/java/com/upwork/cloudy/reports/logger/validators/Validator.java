package com.upwork.cloudy.reports.logger.validators;

@FunctionalInterface
public interface Validator<T, R> {
    R validate(T t);
}
