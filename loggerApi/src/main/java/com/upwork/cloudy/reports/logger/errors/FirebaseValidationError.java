package com.upwork.cloudy.reports.logger.errors;

import java.util.Map;

public final class FirebaseValidationError extends RuntimeException {
    private final String message;

    private final Throwable cause;

    private final Map<String, String> validationErrors;

    public FirebaseValidationError(String message, Throwable cause, Map<String, String> validationErrors) {
        super(message, cause);

        this.message = message;
        this.cause = cause;
        this.validationErrors = validationErrors;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Throwable getCause() {
        return cause;
    }

    public Map<String, String> getValidationErrors() {
        return validationErrors;
    }

    @Override
    public String toString() {
        return "FirebaseValidationError{" +
                "message='" + message + '\'' +
                ", cause=" + cause +
                ", validationErrors=" + validationErrors +
                '}';
    }
}
