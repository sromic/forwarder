package com.upwork.cloudy.reports.logger.services;

public interface LogService<T> {
    void save(T t);

    void update(T t);
}
