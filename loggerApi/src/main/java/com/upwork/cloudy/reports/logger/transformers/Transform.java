package com.upwork.cloudy.reports.logger.transformers;

@FunctionalInterface
public interface Transform<F, T> {
    T transform(F f);
}
