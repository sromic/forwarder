package com.upwork.cloudy.reports.domain.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public final class ApiRequest extends AbstractApi implements Serializable {

    private final String url;

    private final ApiRequestType type;

    private final Map<String, Object> body;

    private final Map<String, String> headers;

    private final Long timestamp;

    @JsonCreator
    public ApiRequest(@JsonProperty("url") String url,
                      @JsonProperty("type") ApiRequestType type,
                      @JsonProperty("body") Map<String, Object> body,
                      @JsonProperty("headers") Map<String, String> headers,
                      @JsonProperty("timestamp") Long timestamp) {
        super(headers, body, timestamp);

        this.url = url;
        this.type = type;
        this.body = body;
        this.headers = headers;
        this.timestamp = timestamp;
    }

    public String getUrl() {
        return url;
    }

    public ApiRequestType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "ApiRequest{" +
                "url='" + url + '\'' +
                ", type=" + type +
                ", body=" + body +
                ", headers=" + headers +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ApiRequest that = (ApiRequest) o;
        return Objects.equals(url, that.url) &&
                type == that.type &&
                Objects.equals(body, that.body) &&
                Objects.equals(headers, that.headers) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), url, type, body, headers, timestamp);
    }
}
