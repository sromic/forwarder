package com.upwork.cloudy.reports.domain.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public final class ApiResponse extends AbstractApi implements Serializable {

    private final Map<String, Object> body;

    private final Map<String, String> headers;

    private final Long timestamp;

    @JsonCreator
    public ApiResponse(@JsonProperty("headers") Map<String, String> headers,
                       @JsonProperty("body") Map<String, Object> body,
                       @JsonProperty("timestamp") Long timestamp) {
        super(headers, body, timestamp);

        this.body = body;
        this.headers = headers;
        this.timestamp = timestamp;
    }

    @Override
    public Map<String, Object> getBody() {
        return body;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "body=" + body +
                ", headers=" + headers +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ApiResponse that = (ApiResponse) o;
        return Objects.equals(body, that.body) &&
                Objects.equals(headers, that.headers) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), body, headers, timestamp);
    }
}
