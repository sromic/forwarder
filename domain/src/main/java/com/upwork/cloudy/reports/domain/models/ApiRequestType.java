package com.upwork.cloudy.reports.domain.models;

public enum ApiRequestType {
    DELETE, GET, POST
}
