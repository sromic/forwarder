package com.upwork.cloudy.reports.domain.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public final class ApiError implements Serializable {
    private final String error;

    private final String url;

    private final HttpStatus status;

    private final Map<String, String> fieldErrors;

    public ApiError(String error, String url, HttpStatus status, Map<String, String> fieldErrors) {
        this.error = error;
        this.url = url;
        this.status = status;
        this.fieldErrors = fieldErrors;
    }

    public String getError() {
        return error;
    }

    public String getUrl() {
        return url;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    @Override
    public String toString() {
        return "ApiError{" +
                "error='" + error + '\'' +
                ", url='" + url + '\'' +
                ", status=" + status +
                ", fieldErrors=" + fieldErrors +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiError apiError = (ApiError) o;
        return Objects.equals(error, apiError.error) &&
                Objects.equals(url, apiError.url) &&
                status == apiError.status &&
                Objects.equals(fieldErrors, apiError.fieldErrors);
    }

    @Override
    public int hashCode() {

        return Objects.hash(error, url, status, fieldErrors);
    }
}
