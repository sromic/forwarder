package com.upwork.cloudy.reports.domain.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public final class ApiEvent implements Serializable {
    final String id;

    final String user;

    final String token;

    final ApiRequest request;

    final ApiResponse response;

    @JsonCreator
    public ApiEvent(@JsonProperty("id") String id,
                    @JsonProperty("user") String user,
                    @JsonProperty("token") String token,
                    @JsonProperty("request") ApiRequest request,
                    @JsonProperty("response") ApiResponse response) {
        this.id = id;
        this.user = user;
        this.token = token;
        this.request = request;
        this.response = response;
    }

    public ApiRequest getRequest() {
        return request;
    }

    public ApiResponse getResponse() {
        return response;
    }

    public String getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "ApiEvent{" +
                "id='" + id + '\'' +
                ", user='" + user + '\'' +
                ", token='" + token + '\'' +
                ", request=" + request +
                ", response=" + response +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiEvent apiEvent = (ApiEvent) o;
        return Objects.equals(id, apiEvent.id) &&
                Objects.equals(user, apiEvent.user) &&
                Objects.equals(token, apiEvent.token) &&
                Objects.equals(request, apiEvent.request) &&
                Objects.equals(response, apiEvent.response);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, user, token, request, response);
    }
}
