package com.upwork.cloudy.reports.domain.models;

import java.util.Map;
import java.util.Objects;

public abstract class AbstractApi {

    protected final Map<String, String> headers;

    protected final Map<String, Object> body;

    protected final Long timestamp;

    public AbstractApi(Map<String, String> headers, Map<String, Object> body, Long timestamp) {
        this.headers = headers;
        this.body = body;
        this.timestamp = timestamp;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Map<String, Object> getBody() {
        return body;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "AbstractApi{" +
                "headers=" + headers +
                ", body=" + body +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractApi that = (AbstractApi) o;
        return Objects.equals(headers, that.headers) &&
                Objects.equals(body, that.body) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {

        return Objects.hash(headers, body, timestamp);
    }
}
