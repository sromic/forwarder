package com.upwork.cloudy.reports.transformers;

@FunctionalInterface
public interface Transform<F, T> {
    T transform(F f);
}
