package com.upwork.cloudy.reports.filters;

import com.upwork.cloudy.reports.domain.models.ApiEvent;
import com.upwork.cloudy.reports.domain.models.ApiRequest;
import com.upwork.cloudy.reports.domain.models.ApiRequestType;
import com.upwork.cloudy.reports.domain.models.ApiResponse;
import com.upwork.cloudy.reports.services.LogService;
import com.upwork.cloudy.reports.utils.Constants.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@WebFilter(urlPatterns = "/*", asyncSupported = true)
@Component
public class AuthFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(AuthFilter.class);

    @Autowired
    private LogService<ApiEvent> logService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOG.info("Starting AuthFilter...");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        final String requestedURL = ((HttpServletRequest) servletRequest).getRequestURL().toString() + "?" + ((HttpServletRequest) servletRequest).getQueryString();

        final HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        final Enumeration<String> headerNames = httpRequest.getHeaderNames();

        final Map<String, String> headers = new HashMap<>();

        if (headerNames != null) {
            while (headerNames.hasMoreElements()) {
                final String headerName = headerNames.nextElement();
                headers.put(headerName, httpRequest.getHeader(headerName));
            }
        }

        final String queryParameters = ((HttpServletRequest) servletRequest).getQueryString();

        final Map<String, String> queryParamsMap = Arrays.stream(queryParameters.split("&")).collect(Collectors.toMap(queryParam -> queryParam.split("=")[0],
                queryParam -> queryParam.indexOf("=") > 0 ? queryParam.split("=")[1] : ""));

        if ((queryParamsMap.containsKey("user") && queryParamsMap.containsKey("token")) || (!queryParamsMap.containsKey("user") && !queryParamsMap.containsKey("token")))
            filterChain.doFilter(servletRequest, servletResponse);

        else {
            final ApiRequest request = new ApiRequest(requestedURL, ApiRequestType.GET, null, headers, System.currentTimeMillis());

            final Map<String, Object> responseBody = new HashMap<>();
            responseBody.put("exception", "Unauthorized exception");
            responseBody.put("code", "401");

            final ApiResponse response = new ApiResponse(headers, responseBody, System.currentTimeMillis());

            final ApiEvent apiEvent = new ApiEvent(UUID.randomUUID().toString(), UserType.ANONYMOUS.toString(), null, request, response);

            //log this to firebase
            logService.save(apiEvent);

            ((HttpServletResponse) servletResponse)
                    .setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }

    }

    @Override
    public void destroy() {
        LOG.info("Stopping AuthFilter...");
    }
}
