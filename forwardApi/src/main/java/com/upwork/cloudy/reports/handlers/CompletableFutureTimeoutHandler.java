package com.upwork.cloudy.reports.handlers;

import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Component
public class CompletableFutureTimeoutHandler {

    private final ScheduledExecutorService scheduler;

    public CompletableFutureTimeoutHandler(ScheduledExecutorService schedule) {
        this.scheduler = schedule;
    }

    public <T> CompletableFuture<T> timeoutAfter(long timeout, TimeUnit unit) {
        final CompletableFuture<T> result = new CompletableFuture<>();

        this.scheduler.schedule(() -> result.completeExceptionally(new TimeoutException()), timeout, unit);

        return result;
    }

    public ScheduledExecutorService getScheduler() {
        return scheduler;
    }
}
