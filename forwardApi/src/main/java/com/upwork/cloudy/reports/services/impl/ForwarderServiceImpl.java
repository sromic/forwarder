package com.upwork.cloudy.reports.services.impl;

import com.upwork.cloudy.reports.configs.ExecutionContextConfig;
import com.upwork.cloudy.reports.domain.models.ApiEvent;
import com.upwork.cloudy.reports.domain.models.ApiResponse;
import com.upwork.cloudy.reports.handlers.CompletableFutureTimeoutHandler;
import com.upwork.cloudy.reports.services.ForwarderService;
import com.upwork.cloudy.reports.services.LogService;
import com.upwork.cloudy.reports.services.RequestService;
import com.upwork.cloudy.reports.transformers.impl.FirebasePayloadTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Service
public class ForwarderServiceImpl implements ForwarderService<ApiEvent, ApiResponse> {

    private final RequestService<ApiEvent> requestService;

    private final LogService<ApiEvent> logService;

    private final CompletableFutureTimeoutHandler timeoutHandler;

    private final FirebasePayloadTransformer firebasePayloadTransformer;

    @Value("${forward.log.timeout}")
    private long logTimeout;

    @Value("${forward.compose.timeout}")
    private long composeTimeout;

    @Autowired
    public ForwarderServiceImpl(RequestService<ApiEvent> requestService, LogService<ApiEvent> logService, CompletableFutureTimeoutHandler timeoutHandler, FirebasePayloadTransformer firebasePayloadTransformer) {
        this.requestService = requestService;
        this.logService = logService;
        this.timeoutHandler = timeoutHandler;
        this.firebasePayloadTransformer = firebasePayloadTransformer;
    }

    @Async(ExecutionContextConfig.TASK_EXECUTOR_SERVICE)
    @Override
    public CompletableFuture<ApiResponse> forward(ApiEvent apiEvent) {

        logService
                .save(apiEvent)
                .acceptEitherAsync(timeoutHandler.timeoutAfter(logTimeout, TimeUnit.SECONDS), response -> {});

        return requestService
                .call(apiEvent)
                .thenCompose(r -> {
                    //transform all keys containing `.` before saving to firebase
                    final Map<String, Object> transformedResponse = firebasePayloadTransformer.transform(r.getResponse().getBody());
                    final ApiResponse updatedApiResponse = new ApiResponse(r.getResponse().getHeaders(), transformedResponse, System.currentTimeMillis());
                    final ApiEvent transformedApiEventWithResponse = new ApiEvent(r.getId(), r.getUser(), r.getToken(), r.getRequest(), updatedApiResponse);

                    return logService.update(transformedApiEventWithResponse);
                })
                .applyToEither(timeoutHandler.timeoutAfter(composeTimeout, TimeUnit.SECONDS), response -> response.getResponse());
    }

    public RequestService<ApiEvent> getRequestService() {
        return requestService;
    }

    public LogService<ApiEvent> getLogService() {
        return logService;
    }

    public CompletableFutureTimeoutHandler getTimeoutHandler() {
        return timeoutHandler;
    }

    public FirebasePayloadTransformer getFirebasePayloadTransformer() {
        return firebasePayloadTransformer;
    }
}
