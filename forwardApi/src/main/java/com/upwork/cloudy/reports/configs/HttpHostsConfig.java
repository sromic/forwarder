package com.upwork.cloudy.reports.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "http-conn-pool")
public class HttpHostsConfig {

    private Integer maxTotal;
    private Integer maxPerRoute;

    public Integer getMaxTotal() {
        return this.maxTotal;
    }

    public void setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
    }

    public Integer getMaxPerRoute() {
        return this.maxPerRoute;
    }

    public void setMaxPerRoute(Integer defaultMaxPerRoute) {
        this.maxPerRoute = defaultMaxPerRoute;
    }
}
