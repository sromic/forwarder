package com.upwork.cloudy.reports.transformers.impl;

import com.upwork.cloudy.reports.transformers.Transform;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class FirebasePayloadTransformer implements Transform<Map<String, Object>, Map<String, Object>> {

    @Override
    public Map<String, Object> transform(final Map<String, Object> f) {

        final Map<String, Object> updatedMap = new HashMap<>();

        f.keySet().stream().forEach(k -> {

            if (k.contains("."))
                updatedMap.put(k.replaceAll("\\.", "____"), f.get(k));

            else if (f.get(k) instanceof Map)
                updatedMap.put(k, transform((Map<String, Object>) f.get(k)));

            else
                updatedMap.put(k, f.get(k));
        });

        return updatedMap;
    }
}
