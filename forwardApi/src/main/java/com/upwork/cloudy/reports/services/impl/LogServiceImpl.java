package com.upwork.cloudy.reports.services.impl;

import com.upwork.cloudy.reports.configs.ExecutionContextConfig;
import com.upwork.cloudy.reports.domain.models.ApiEvent;
import com.upwork.cloudy.reports.services.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class LogServiceImpl implements LogService<ApiEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(LogServiceImpl.class);

    private final RestTemplate restTemplate;

    @Value("${logger.api.url}")
    private String loggerApiUrl;

    @Autowired
    public LogServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Async(ExecutionContextConfig.TASK_EXECUTOR_SERVICE)
    @Override
    public CompletableFuture<ApiEvent> save(ApiEvent apiEvent) {
        LOG.info("Calling loggerApi for saving api event {}", apiEvent);

        return CompletableFuture.supplyAsync(() -> {
            restTemplate.postForEntity(loggerApiUrl, apiEvent, Void.class);

            return apiEvent;
        });
    }

    @Async(ExecutionContextConfig.TASK_EXECUTOR_SERVICE)
    @Override
    public CompletableFuture<ApiEvent> update(ApiEvent apiEvent) {
        LOG.info("Calling loggerApi for updating api event {}", apiEvent);

        return CompletableFuture.supplyAsync(() -> {
            restTemplate.postForEntity(loggerApiUrl, apiEvent, Void.class);

            return apiEvent;
        });
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public String getLoggerApiUrl() {
        return loggerApiUrl;
    }
}
