package com.upwork.cloudy.reports.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    @GetMapping(value = "/correct", produces = "application/json")
    @ResponseStatus(code = HttpStatus.OK)
    public String call200() {

        return "{\"a\": \"I am returning some dummy value\"}";
    }

    @GetMapping(value = "/incorrect", produces = "application/json")
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public String call500() {
        return "{\"error\": \"Exception from server\"}";
    }

    @DeleteMapping(value = "/correct", produces = "application/json")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void callDelete200() {
    }

    @PostMapping(value = "/correct", produces = "application/json")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public String callPost200(@RequestBody final String payload) {
        return "{\"a\": \"payload accepted\"}";
    }
}
