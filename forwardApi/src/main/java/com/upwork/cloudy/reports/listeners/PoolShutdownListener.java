package com.upwork.cloudy.reports.listeners;

import com.upwork.cloudy.reports.configs.ExecutionContextConfig;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledExecutorService;

@Component
public final class PoolShutdownListener implements ApplicationListener<ContextClosedEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(PoolShutdownListener.class);

    private final ThreadPoolTaskExecutor controllerThreadPoolExecutor;

    private final ThreadPoolTaskExecutor serviceThreadPoolExecutor;

    private final PoolingHttpClientConnectionManager poolingHttpClientConnectionManager;

    private final ScheduledExecutorService scheduledThreadPoolExecutor;

    @Autowired
    public PoolShutdownListener(@Qualifier(ExecutionContextConfig.TASK_EXECUTOR_CONTROLLER) ThreadPoolTaskExecutor threadPoolExecutor,
                                @Qualifier(ExecutionContextConfig.TASK_EXECUTOR_SERVICE) ThreadPoolTaskExecutor serviceThreadPoolExecutor,
                                PoolingHttpClientConnectionManager poolingHttpClientConnectionManager,
                                @Qualifier(ExecutionContextConfig.TASK_EXECUTOR_SCHEDULER) ScheduledExecutorService scheduledThreadPoolExecutor) {
        this.controllerThreadPoolExecutor = threadPoolExecutor;

        this.serviceThreadPoolExecutor = serviceThreadPoolExecutor;

        this.poolingHttpClientConnectionManager = poolingHttpClientConnectionManager;

        this.scheduledThreadPoolExecutor = scheduledThreadPoolExecutor;
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent applicationReadyEvent) {
        LOG.info("Application is shutting down!");

        if (this.controllerThreadPoolExecutor != null) {
            LOG.info("Shutting down controller thread pool executor gracefully!");

            this.controllerThreadPoolExecutor.shutdown();
        }

        if (this.serviceThreadPoolExecutor != null) {
            LOG.info("Shutting down service thread pool executor gracefully!");

            this.serviceThreadPoolExecutor.shutdown();
        }

        if (this.poolingHttpClientConnectionManager != null) {
            LOG.info("Shutting down http client connection manager pool!");

            this.poolingHttpClientConnectionManager.shutdown();
        }

        if (this.scheduledThreadPoolExecutor != null) {
            LOG.info("Shutting down ScheduledThreadPoolExecutor gracefully!");

            this.scheduledThreadPoolExecutor.shutdown();
        }
    }
}
