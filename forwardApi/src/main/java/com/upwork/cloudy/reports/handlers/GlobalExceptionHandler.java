package com.upwork.cloudy.reports.handlers;

import com.upwork.cloudy.reports.domain.models.ApiError;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;
import java.util.stream.Collectors;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatchException(HttpServletRequest request, MethodArgumentTypeMismatchException ex) {

        final String requestedURL = request.getRequestURL().toString() + "?" + request.getQueryString();

        return buildResponseEntity(new ApiError(ex.getMessage(), requestedURL, HttpStatus.BAD_REQUEST, null));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolationException(HttpServletRequest request, ConstraintViolationException ex) {

        final Set<ConstraintViolation<?>> bindingResult = ex.getConstraintViolations();

        final String apiConstraintsError = bindingResult
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining(","));

        final String requestedURL = request.getRequestURL().toString() + "?" + request.getQueryString();

        return buildResponseEntity(new ApiError(apiConstraintsError, requestedURL, HttpStatus.BAD_REQUEST, null));
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleException(HttpServletRequest request, Exception ex) {
        final String requestedURL = request.getRequestURL().toString() + "?" + request.getQueryString();

        return buildResponseEntity(new ApiError(ex.getMessage(), requestedURL, HttpStatus.INTERNAL_SERVER_ERROR, null));
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}
