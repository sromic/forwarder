package com.upwork.cloudy.reports.services;

import java.util.concurrent.CompletableFuture;

public interface LogService<T> {

    CompletableFuture<T> save(T t);

    CompletableFuture<T> update(T t);
}
