package com.upwork.cloudy.reports.configs;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

@Configuration
public class ExecutionContextConfig implements AsyncConfigurer {

    @Value("${forward.thread.core-pool}")
    private int corePoolSize;

    @Value("${forward.thread.max-pool}")
    private int maxPoolSize;

    @Value("${forward.queue.capacity}")
    private int queueCapacity;

    @Value("${forward.thread.timeout}")
    private int threadTimeout;

    @Value("${scheduler.thread.core-pool}")
    private int schedulerCorePoolSize;

    public static final String TASK_EXECUTOR_SERVICE = "serviceTaskExecutor";

    public static final String TASK_EXECUTOR_CONTROLLER = "controllerTaskExecutor";

    public static final String TASK_EXECUTOR_SCHEDULER = "scheduler";

    @Bean(name = TASK_EXECUTOR_SERVICE)
    public ThreadPoolTaskExecutor serviceThreadPoolTaskExecutor() {
       return createExecutor(TASK_EXECUTOR_SERVICE + "-");
    }

    @Bean(name = TASK_EXECUTOR_CONTROLLER)
    public ThreadPoolTaskExecutor contollerThreadPoolTaskExecutor() {
       return createExecutor(TASK_EXECUTOR_CONTROLLER + "-");
    }

    private ThreadPoolTaskExecutor createExecutor(final String prefix) {
        final ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
        threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);
        threadPoolTaskExecutor.setQueueCapacity(queueCapacity);
        threadPoolTaskExecutor.setKeepAliveSeconds(threadTimeout);
        threadPoolTaskExecutor.setThreadNamePrefix(prefix);
        threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        threadPoolTaskExecutor.initialize();

        return threadPoolTaskExecutor;
    }

    @Bean(name = TASK_EXECUTOR_SCHEDULER)
    public ScheduledExecutorService scheduledThreadPoolExecutor() {
        final ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(schedulerCorePoolSize);

        return scheduledThreadPoolExecutor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }
}
