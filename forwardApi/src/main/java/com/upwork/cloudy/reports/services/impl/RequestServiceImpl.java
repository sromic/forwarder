package com.upwork.cloudy.reports.services.impl;

import com.upwork.cloudy.reports.configs.ExecutionContextConfig;
import com.upwork.cloudy.reports.domain.models.ApiEvent;
import com.upwork.cloudy.reports.domain.models.ApiRequestType;
import com.upwork.cloudy.reports.domain.models.ApiResponse;
import com.upwork.cloudy.reports.services.RequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service
public class RequestServiceImpl implements RequestService<ApiEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(RequestServiceImpl.class);

    private final RestTemplate restTemplate;

    @Autowired
    public RequestServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Async(ExecutionContextConfig.TASK_EXECUTOR_SERVICE)
    @Override
    public CompletableFuture<ApiEvent> call(ApiEvent apiEvent) {
        LOG.info("Calling HTTP {} url {}", apiEvent.getRequest().getType(), apiEvent.getRequest().getUrl());

        final CompletableFuture<ResponseEntity<Map<String, Object>>> response = exchange(apiEvent);

        return response
        .thenApply(r -> {
            final Map<String, String> responseHttpHeaders = r.getHeaders().toSingleValueMap();
            final ApiResponse apiResponse = new ApiResponse(responseHttpHeaders, r.getBody(), System.currentTimeMillis());

            LOG.info("Get response HTTP {} from url {}: {}", apiEvent.getRequest().getType(), apiEvent.getRequest().getUrl(), r.getBody());

            return new ApiEvent(apiEvent.getId(), apiEvent.getUser(), apiEvent.getToken(), apiEvent.getRequest(), apiResponse);
        })
        .exceptionally(ex -> {
            if (ex instanceof HttpStatusCodeException) {
                final HttpStatusCodeException e = (HttpStatusCodeException) ex;

                LOG.error("HttpStatusCodeException while calling HTTP {} url {}: {}", apiEvent.getRequest().getType(), apiEvent.getRequest().getUrl(), e.getResponseBodyAsString());

                final Map<String, String> httpResponseHeaders = e.getResponseHeaders().toSingleValueMap();
                final Map<String, Object> body = new HashMap<>();
                body.put("exception", e);

                final ApiResponse apiResponse = new ApiResponse(httpResponseHeaders, body, System.currentTimeMillis());

                return new ApiEvent(apiEvent.getId(), apiEvent.getUser(), apiEvent.getToken(), apiEvent.getRequest(), apiResponse);
            }

            else if (ex instanceof ResourceAccessException) {
                LOG.error("ResourceAccessException while calling HTTP {} url {}: {}", apiEvent.getRequest().getType(), apiEvent.getRequest().getUrl(), ex.getMessage());

                final Map<String, Object> body = new HashMap<>();
                body.put("exception", ex.getMessage());

                final ApiResponse apiResponse = new ApiResponse(null, body, System.currentTimeMillis());

                return new ApiEvent(apiEvent.getId(), apiEvent.getUser(), apiEvent.getToken(), apiEvent.getRequest(), apiResponse);
            }

            else {
                LOG.error("Exception while calling HTTP {} url {}: {}", apiEvent.getRequest().getType(), apiEvent.getRequest().getUrl(), ex.getMessage());

                final Map<String, Object> body = new HashMap<>();
                body.put("exception", ex.getMessage());

                final ApiResponse apiResponse = new ApiResponse(null, body, System.currentTimeMillis());

                return new ApiEvent(apiEvent.getId(), apiEvent.getUser(), apiEvent.getToken(), apiEvent.getRequest(), apiResponse);
            }
        });
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    private CompletableFuture<ResponseEntity<Map<String, Object>>> exchange(final ApiEvent apiEvent) {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAll(apiEvent.getRequest().getHeaders());

        final ParameterizedTypeReference<Map<String, Object>> typeRef = new ParameterizedTypeReference<Map<String, Object>>() {};

        if (ApiRequestType.GET == apiEvent.getRequest().getType()) {
            final HttpEntity<String> requestEntity = new HttpEntity<>(null, httpHeaders);

            return CompletableFuture.supplyAsync(() -> restTemplate.exchange(
                    apiEvent.getRequest().getUrl(),
                    HttpMethod.GET,
                    requestEntity,
                    typeRef
            ));
        }
        else if (ApiRequestType.DELETE == apiEvent.getRequest().getType()) {
            final HttpEntity<String> requestEntity = new HttpEntity<>(null, httpHeaders);

            return CompletableFuture.supplyAsync(() -> restTemplate.exchange(
                    apiEvent.getRequest().getUrl(),
                    HttpMethod.DELETE,
                    requestEntity,
                    typeRef
            ));
        }
        else {
            final HttpEntity<Map<String, Object>> requestEntity = new HttpEntity<>(apiEvent.getRequest().getBody(), httpHeaders);

            return CompletableFuture.supplyAsync(() -> restTemplate.exchange(
                    apiEvent.getRequest().getUrl(),
                    HttpMethod.POST,
                    requestEntity,
                    typeRef
            ));
        }
    }
}
