package com.upwork.cloudy.reports.controllers;

import com.upwork.cloudy.reports.configs.ExecutionContextConfig;
import com.upwork.cloudy.reports.domain.models.ApiEvent;
import com.upwork.cloudy.reports.domain.models.ApiRequest;
import com.upwork.cloudy.reports.domain.models.ApiRequestType;
import com.upwork.cloudy.reports.domain.models.ApiResponse;
import com.upwork.cloudy.reports.services.ForwarderService;
import com.upwork.cloudy.reports.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URL;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RestController
@Validated
public class ForwardController {

    @Autowired
    private ForwarderService<ApiEvent, ApiResponse> forwarderService;

    @Async(ExecutionContextConfig.TASK_EXECUTOR_CONTROLLER)
    @GetMapping(value = "/")
    public CompletableFuture<ResponseEntity<?>> forwardRequest(@RequestParam("url") final URL url,
                                                               @RequestParam(value = "user", required = false) final Optional<String> user,
                                                               @RequestParam(value = "token", required = false) final String token,
                                                               @RequestHeader final Map<String, String> headers) {

        final ApiRequest request = new ApiRequest(url.toString(), ApiRequestType.GET, null, headers, System.currentTimeMillis());
        final ApiEvent apiEvent = new ApiEvent(UUID.randomUUID().toString(), user.orElse(Constants.UserType.ANONYMOUS.toString()), token, request, null);
        final CompletableFuture<ApiResponse> apiResponseFuture = forwarderService.forward(apiEvent);

        return apiResponseFuture
                .thenApply(r -> new ResponseEntity<>(r.getBody(), HttpStatus.OK));
    }

    @Async(ExecutionContextConfig.TASK_EXECUTOR_CONTROLLER)
    @DeleteMapping(value = "/", produces = "application/json; charset=utf-8")
    public CompletableFuture<ResponseEntity> deleteForwardRequest(@RequestParam("url") final URL url,
                                                                  @RequestParam(value = "user", required = false) final Optional<String> user,
                                                                  @RequestParam(value = "token", required = false) final String token,
                                                                  @RequestHeader final Map<String, String> headers) {

        final ApiRequest request = new ApiRequest(url.toString(), ApiRequestType.DELETE, null, headers, System.currentTimeMillis());
        final ApiEvent apiEvent = new ApiEvent(UUID.randomUUID().toString(), user.orElse(Constants.UserType.ANONYMOUS.toString()), token, request, null);
        final CompletableFuture<ApiResponse> apiResponseFuture = forwarderService.forward(apiEvent);

        return apiResponseFuture
                .thenApply(r -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @Async(ExecutionContextConfig.TASK_EXECUTOR_CONTROLLER)
    @PostMapping(value = "/", produces = "application/json; charset=utf-8")
    public CompletableFuture<ResponseEntity<?>> postForwardRequest(@RequestBody Map<String, Object> payload,
                                                                   @RequestParam("url") final URL url,
                                                                   @RequestParam(value = "user", required = false) final Optional<String> user,
                                                                   @RequestParam(value = "token", required = false) final String token,
                                                                   @RequestHeader final Map<String, String> headers) {

        final ApiRequest request = new ApiRequest(url.toString(), ApiRequestType.POST, payload, headers, System.currentTimeMillis());
        final ApiEvent apiEvent = new ApiEvent(UUID.randomUUID().toString(), user.orElse(Constants.UserType.ANONYMOUS.toString()), token, request, null);
        final CompletableFuture<ApiResponse> apiResponseFuture = forwarderService.forward(apiEvent);

        return apiResponseFuture
                .thenApply(r -> new ResponseEntity<>(r.getBody(), HttpStatus.CREATED));
    }

}