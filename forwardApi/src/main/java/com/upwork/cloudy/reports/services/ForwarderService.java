package com.upwork.cloudy.reports.services;

import com.upwork.cloudy.reports.domain.models.ApiResponse;

import java.util.concurrent.CompletableFuture;

public interface ForwarderService<R, T> {
    CompletableFuture<ApiResponse> forward(R request);
}
