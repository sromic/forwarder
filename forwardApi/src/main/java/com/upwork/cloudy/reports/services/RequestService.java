package com.upwork.cloudy.reports.services;

import java.util.concurrent.CompletableFuture;

public interface RequestService<T> {
    CompletableFuture<T> call(T apiEvent);
}