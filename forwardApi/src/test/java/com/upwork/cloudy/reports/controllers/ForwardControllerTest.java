package com.upwork.cloudy.reports.controllers;

import com.upwork.cloudy.reports.Boot;
import com.upwork.cloudy.reports.domain.models.ApiEvent;
import com.upwork.cloudy.reports.domain.models.ApiResponse;
import com.upwork.cloudy.reports.services.ForwarderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Boot.class)
public class ForwardControllerTest {

    @Autowired
    private ForwardController forwardController;

    private MockMvc mvc;

    @MockBean
    private ForwarderService<ApiEvent, ApiResponse> forwarderService;

    private HttpHeaders httpHeaders;

    private long timestamp;

    private String user;

    private String token;

    @Before
    public void setup() {

        this.mvc = MockMvcBuilders
                .standaloneSetup(forwardController)
                .build();

        this.httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        this.timestamp = System.currentTimeMillis();

        this.user = "user";

        this.token = UUID.randomUUID().toString();
    }

    @Test
    public void serverOkUrlTest() throws Exception {

        final String url = "http://localhost:8080/correct";

        final MvcResult mvcResult = mvc
                .perform(get("?url="+url+"&user="+this.user+"&token="+this.token).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.request().asyncStarted())
                .andReturn();

        final Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("response", "I am returning some dummy value");

        when(this.forwarderService.forward(any(ApiEvent.class)))
                .thenReturn(CompletableFuture.completedFuture(new ApiResponse(this.httpHeaders.toSingleValueMap(), responseBody, this.timestamp)));
        try {
            this.mvc.perform(asyncDispatch(mvcResult))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(content().json("{'response':'I am returning some dummy value'}"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void serverErrorUrlTest() throws Exception {

        final String url = "http://localhost:8080/incorrect";

        final MvcResult mvcResult = mvc
                .perform(get("?url="+url+"&user="+this.user+"&token="+this.token).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.request().asyncStarted())
                .andReturn();

        final Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("response", "Exception from server");

        when(this.forwarderService.forward(any(ApiEvent.class)))
                .thenReturn(CompletableFuture.completedFuture(new ApiResponse(this.httpHeaders.toSingleValueMap(), responseBody, this.timestamp)));

        try {
            this.mvc.perform(asyncDispatch(mvcResult))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(content().json("{'response':'Exception from server'}"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void validGETUrlTest() throws Exception {

        final String url = "http://localhost:8080/correct";

        final MvcResult mvcResult = mvc
                .perform(get("?url="+url+"&user="+this.user+"&token="+this.token).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.request().asyncStarted())
                .andReturn();

        final Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("response", "I am returning some dummy value");

        when(this.forwarderService.forward(any(ApiEvent.class)))
                .thenReturn(CompletableFuture.completedFuture(new ApiResponse(this.httpHeaders.toSingleValueMap(), responseBody, this.timestamp)));

        try {
            this.mvc.perform(asyncDispatch(mvcResult))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(content().json("{'response':'I am returning some dummy value'}"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void invalidGETUrlTest() throws Exception {

        final String url = "invalidURLAddress";

        final MvcResult mvcResult = mvc
                .perform(get("?url="+url+"&user="+this.user+"&token="+this.token).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.request().asyncNotStarted())
                .andReturn();

        try {
            this.mvc.perform(asyncDispatch(mvcResult))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(content().json("{\n" +
                            "    \"error\": \"Failed to convert value of type 'java.lang.String' to required type 'java.net.URL'; nested exception is java.lang.IllegalArgumentException: Could not retrieve URL for class path resource [invalidURLAddress]: class path resource [invalidURLAddress] cannot be resolved to URL because it does not exist\",\n" +
                            "    \"url\": \"http://localhost?url=invalidURLAddress\",\n" +
                            "    \"status\": \"BAD_REQUEST\"\n" +
                            "}"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void validDELETEUrlTest() throws Exception {

        final String url = "http://localhost:8080/correct";

        final MvcResult mvcResult = mvc
                .perform(delete("?url="+url+"&user="+this.user+"&token="+this.token).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.request().asyncStarted())
                .andReturn();

        try {
            this.mvc.perform(asyncDispatch(mvcResult))
                    .andExpect(status().isNoContent())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void invalidDELETEUrlTest() throws Exception {

        final String url = "invalidURLAddress";

        final MvcResult mvcResult = mvc
                .perform(delete("?url="+url+"&user="+this.user+"&token="+this.token).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.request().asyncNotStarted())
                .andReturn();

        try {
            this.mvc.perform(asyncDispatch(mvcResult))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(content().json("{\n" +
                            "    \"error\": \"Failed to convert value of type 'java.lang.String' to required type 'java.net.URL'; nested exception is java.lang.IllegalArgumentException: Could not retrieve URL for class path resource [invalidURLAddress]: class path resource [invalidURLAddress] cannot be resolved to URL because it does not exist\",\n" +
                            "    \"url\": \"http://localhost?url=invalidURLAddress\",\n" +
                            "    \"status\": \"BAD_REQUEST\"\n" +
                            "}"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void validPOSTUrlTest() throws Exception {

        final String url = "http://localhost:8080/correct";

        final MvcResult mvcResult = mvc
                .perform(post("?url="+url+"&user="+this.user+"&token="+this.token)
                        .accept(MediaType.APPLICATION_JSON)
                        .content("{\"x\": 1}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.request().asyncStarted())
                .andReturn();

        final Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("response", "I am returning some dummy value");

        when(this.forwarderService.forward(any(ApiEvent.class)))
                .thenReturn(CompletableFuture.completedFuture(new ApiResponse(this.httpHeaders.toSingleValueMap(), responseBody, this.timestamp)));

        try {
            this.mvc.perform(asyncDispatch(mvcResult))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(status().isCreated())
                    .andExpect(content().json("{'response':'I am returning some dummy value'}"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void invalidPOSTUrlTest() throws Exception {

        final String url = "invalidURLAddress";

        final MvcResult mvcResult = mvc
                .perform(post("?url="+url+"&user="+this.user+"&token="+this.token)
                        .accept(MediaType.APPLICATION_JSON)
                        .content("{\"x\": 1}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.request().asyncNotStarted())
                .andReturn();

        try {
            this.mvc.perform(asyncDispatch(mvcResult))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().json("{\n" +
                            "    \"error\": \"Failed to convert value of type 'java.lang.String' to required type 'java.net.URL'; nested exception is java.lang.IllegalArgumentException: Could not retrieve URL for class path resource [invalidURLAddress]: class path resource [invalidURLAddress] cannot be resolved to URL because it does not exist\",\n" +
                            "    \"url\": \"http://localhost?url=invalidURLAddress\",\n" +
                            "    \"status\": \"BAD_REQUEST\"\n" +
                            "}"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}